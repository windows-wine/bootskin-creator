; � Drugwash, March 2014
; v1.0.0
;================================================
;	FULLSCREEN - STORE CURRENT WINDOW POSITION
;================================================
FullScreen_Store(hwnd, set="")
{
Static
if !hwnd
	return
if set in C,S					; Set values
	WinGetPos, X, Y, W, H, ahk_id %hwnd%
return X "x" Y "x" W "x" H
}
;================================================
;	FULLSCREEN - RESTORE PREVIOUS GRAPHICS MODE
;================================================
FullScreen_Reset(hwnd, device)
{
Global AStr
if !device
	return
DllCall("ChangeDisplaySettingsExA"
	, AStr, device
	, "UInt", NULL
	, "UInt", NULL
	, "UInt", 0x4				; CDS_FULLSCREEN
	, "UInt", NULL)
r := FullScreen_Store(hwnd)		; get old window position
StringSplit, old, r, x
DllCall("SetWindowPos"
	, "UInt", hwnd
	, "UInt", 0
	, "Int", old1
	, "Int", old2
	, "Int", 0
	, "Int", 0
	, "UInt", 0x5)				; SWP_NOSIZE SWP_NOZORDER
}
;================================================
;	FULLSCREEN - APPLY SELECTED GRAPHICS MODE (TEMPORARY)
;================================================
FullScreen_Set(hwnd, param="640x480x8x60", bord=0, dbg=0)
{
Global AStr
Static DMsz, errlist="Driver failed to set graphics mode,Graphics mode not supported
,Cannot write to registry,Invalid flags,Invalid parameter/flags,System is DualView"
#LTrim
StringSplit, v, param, x
; check for min/max width, height, bpp, refresh and monitor
if (!hwnd OR !v1 OR !v2)
	return
if !v5					; if no display was specified, detect and use current
	{
	hMon := DllCall("MonitorFromWindow"
				, "UInt", hwnd
				, "UInt", 0x1)			; MONITOR_DEFAULTTOPRIMARY
;				, "UInt", 0x2)			; MONITOR_DEFAULTTONEAREST
	MIsz := 72						; MONITORINFOEX struct size (104 U)
	VarSetCapacity(mi, MIsz, 0)
	NumPut(MIsz, mi, 0, "UInt")
	DllCall("GetMonitorInfoA"
		, "UInt", hMon
		, "UInt", &mi)
	device := DllCall("MulDiv"
				, "UInt", &mi+40
				, "Int", 1
				, "Int", 1
				, AStr)
	monX := NumGet(mi, 4, "Int")			; top-left monitor coordinates
	monY := NumGet(mi, 8, "Int")			; can be negative values
	VarSetCapacity(mi, 0)				; free memory
	}
else
	{
	; get device name for selected monitor
	}
;msgbox, Device=%device%`nHandle=%hMon%`nX=%monX% Y=%monY%
if !device
	return
FullScreen_Store(hwnd, "S")				; store old window position
; DEVMODE structure size 148 (Win95) 156 (WinNT4/2000)
if !DMsz
	DMsz := A_OSType="WIN32_NT" ? 156 : 148
VarSetCapacity(DEVMODE, DMsz, 0)		; allocate space for the DEVMODE struct
NumPut(DMsz, DEVMODE, 36, "UShort")	; set struct size
if !DllCall("EnumDisplaySettingsA"
		, AStr, device
		, "UInt", -1					; ENUM_CURRENT_SETTINGS
		, "UInt", &DEVMODE)
	{
	if dbg
		MsgBox, 0x3010, Display error,
			(
			Error %ErrorLevel% in EnumDisplaySettings()
			)
	return
	}
flags := 0x80000 | 0x100000				; DM_PELSWIDTH DM_PELSHEIGHT
NumPut(v1, DEVMODE, 108, "UInt")		; dmPelsWidth
NumPut(v2, DEVMODE, 112, "UInt")		; dmPelsHeight
if v3
	{
	NumPut(v3, DEVMODE, 104, "UInt")	; dmBitsPerPel
	flags += 0x40000					; DM_BITSPERPEL
	}
else v3 := NumGet(DEVMODE, 104, "UInt")
if v4
	{
	NumPut(v4, DEVMODE, 120, "UInt")	; dmDisplayFrequency
	flags += 0x400000					; DM_DISPLAYFREQUENCY
	}
else v4 := NumGet(DEVMODE, 120, "UInt")
NumPut(flags, DEVMODE, 40, "UInt")		; dmFields 
if err := DllCall("ChangeDisplaySettingsExA"
			, AStr, device
			, "UInt", &DEVMODE
			, "UInt", NULL
			, "UInt", 0x2				; CDS_TEST
			, "UInt", NULL)
	{
	if err=1							; DISP_CHANGE_RESTART
		err := "Requires system restart"
	else Loop, Parse, errlist, CSV
		if (A_Index=Abs(err))
			{
			err := A_LoopField
			break
			}
	if dbg
		{
		MsgBox, 0x3014, Fullscreen test,
			(
			Test failed for %v1%x%v2%x%v3%bpp @ %v4% Hz.
			Error: %err%.
			Do you want to continue?
			)
		IfMsgBox No
			return
		}
	else return
	}
else
	{
	if dbg
		{
		MsgBox, 0x3024, Fullscreen test,
			(
			Test succeeded for %v1%x%v2%x%v3%bpp @ %v4% Hz.
			Apply selected mode now?
			)
		IfMsgBox No
			return
		}
	NumPut(DMsz, DEVMODE, 36, "UShort")	; set struct size
	}
if err := DllCall("ChangeDisplaySettingsExA"
			, AStr, device
			, "UInt", &DEVMODE
			, "UInt", NULL
			, "UInt", 0x4				; CDS_FULLSCREEN
			, "UInt", NULL)
	{
	if err = 1							; DISP_CHANGE_RESTART
		{
		if dbg
			{
			MsgBox, 0x3014, Display mode change,
				(
				Changing display mode requires computer restart.
				Perform restart now?
				)
			IfMsgBox  Yes
				Shutdown, 2
			}
		return DllCall("ChangeDisplaySettingsExA"
					, "UInt", NULL
					, "UInt", NULL
					, "UInt", NULL
					, "UInt", 0x4		; CDS_FULLSCREEN
					, "UInt", NULL)
		}
	else Loop, Parse, errlist, CSV
		if (A_Index=Abs(err))
			{
			err := A_LoopField
			break
			}
	if dbg
		{
		MsgBox, 0x3010, Display error,
			(
			Cannot change display mode to fullscreen.
			Error: %err%.
			)
		}
	return DllCall("ChangeDisplaySettingsExA"
				, "UInt", NULL
				, "UInt", NULL
				, "UInt", NULL
				, "UInt", 0x4			; CDS_FULLSCREEN
				, "UInt", NULL)
	}
NumPut(MIsz, mi, 0, "UInt")				; retrieve new monitor coordinates
DllCall("GetMonitorInfoA"
	, "UInt", hMon
	, "UInt", &mi)
monX := NumGet(mi, 4, "Int")				; top-left monitor coordinates
monY := NumGet(mi, 8, "Int")				; can be negative values
VarSetCapacity(mi, 0)					; free memory
DllCall("SetWindowPos"
	, "UInt", hwnd
	, "UInt", 0						; HWND_TOP
	, "UInt", monX-bord
	, "UInt", monY-bord
	, "Int", 0
	, "Int", 0
	, "UInt", 0x5)						; SWP_NOSIZE SWP_NOZORDER
return device
}
