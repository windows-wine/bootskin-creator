; � Drugwash, March 2014
; v1.0.0
;==========================================================
;
;==========================================================
PaletteStore(ByRef a, ByRef s, ByRef g, ByRef b, ByRef bc, ByRef sc, ByRef r, store=0)
{
Static rect, v1, v2, v3, v4, v5, v6, v7
if store
	{
	VarSetCapacity(rect, 16, 0)
	v1 := a, v2 := s, v3 := g, v4 := b, v5 := bc, v6 := sc, v7 := &rect
	}
else
	a := v1, s := v2, g := v3, b := v4, bc := v5, sc := v6, r := v7
}
;==========================================================
;
;==========================================================
PaletteSelect(array, size, gap)
{
PaletteStore(array, size, gap, u1, u2, u3, rect)	; retrieve values
MouseGetPos, x, y,, hwnd, 2
ControlGetPos, cx, cy,,,, ahk_id %hwnd%
x-=cx, y-=cy
StringSplit, a, array, x, %A_Space%%A_Tab%
StringSplit, s, size, x, %A_Space%%A_Tab%
StringSplit, g, gap, x, %A_Space%%A_Tab%
c := Floor((x-g2)/(s1+4+g1)), r := Floor((y-g2)/(s2+4+g1))
i := g2+c*(s1+4+g1)-1
j := g2+r*(s2+4+g1)-1
if (a1*r+c+1>a3) OR (x<i+2) OR (x>i+s1+3) OR (y<j+2) OR (y>j+s2+3)
	OR (c>a1-1) OR (r>a2-1) OR (c<0) OR (r<0)
	return 0
;msgbox, array=%array% size=%size% gap=%gap% rect=%rect%
NumPut(i, rect+0, 0, "Int")
NumPut(j, rect+0, 4, "Int")
NumPut(i+s1+4, rect+0, 8, "Int")
NumPut(j+s2+4, rect+0, 12, "Int")
DllCall("InvalidateRect", "UInt", hwnd, "UInt", 0, "UInt", 0)
return a1*r+c+1	; return color index only if click position is valid
}
;==========================================================
;
;==========================================================
PalettePaint(hwnd)
{
PaletteStore(array, size, gap, buf, bkg, col, rect)					; retrieve values
StringSplit, a, array, x, %A_Space%%A_Tab%
StringSplit, s, size, x, %A_Space%%A_Tab%
StringSplit, g, gap, x, %A_Space%%A_Tab%
x := g2+1, y := g2+1, b := 1								; b=static border
WinGetPos,,, ww, wh, ahk_id %hwnd%
VarSetCapacity(ps, 64, 0)									; PAINTSTRUCT struct
hDC := DllCall("BeginPaint", "UInt", hwnd, "UInt", &ps)
hDCm := DllCall("CreateCompatibleDC", "UInt", 0)
hBmp := DllCall("CreateCompatibleBitmap"
			, "UInt", hDC
			, "UInt", ww-2*b
			, "UInt", wh-2*b)
hOld	 := DllCall("SelectObject", "UInt", hDCm, "UInt", hBmp)
DllCall("SetStretchBltMode", "UInt", hDCm, "UInt", 4)
hBrush := DllCall("CreateSolidBrush", "UInt", bkg)					; create background brush
hPBrush := DllCall("SelectObject", "UInt", hDCm, "UInt", hBrush)		; get original brush
DllCall("PatBlt"
	, "UInt", hDCm
	, "Int", 0
	, "Int", 0
	, "UInt", ww-2*b
	, "UInt", wh-2*b
	, "UInt", 0xF00021)	; PATCOPY 							 fill background
DllCall("DeleteObject", "UInt", hBrush)							; delete background brush
Loop, % a1*a2
	{
	if (!Mod(A_Index-1, a1) && A_Index>a1)
		{
		x := g2+1
		y+=s2+g1+4
		}
	color	 := A_Index > a3 ? bkg : NumGet(buf+0, 4*(A_Index-1), "UInt")	; get color information from buffer
	hBrush := DllCall("CreateSolidBrush", "UInt", color)			; create new brush
	DllCall("DeleteObject"
		, "UInt", DllCall("SelectObject"
					, "UInt", hDCm
					, "UInt", hBrush))						; select new & delete previous brush
	DllCall("PatBlt"
		, "UInt", hDCm
		, "Int", x
		, "Int", y
		, "UInt", s1
		, "UInt", s2
		, "UInt", 0xF00021)	; PATCOPY paint rectangle
	x+=s1+g1+4
	}
hBrush := DllCall("CreateSolidBrush", "UInt", col)					; create selection brush
DllCall("DeleteObject"
	, "UInt", DllCall("SelectObject"
				, "UInt", hDCm
				, "UInt", hBrush))							; select new & delete previous brush
DllCall("FrameRect"
	, "UInt", hDCm
	, "UInt", rect
	, "UInt", hBrush)										; draw selection rectangle
DllCall("DeleteObject"
	, "UInt", DllCall("SelectObject"
				, "UInt", hDCm
				, "UInt", hPBrush))							; delete selection brush
DllCall("BitBlt"
	, "UInt", hDC
	, "Int", 0
	, "Int", 0
	, "Int", ww-2*b
	, "Int", wh-2*b
	, "UInt", hDCm
	, "Int", 0
	, "Int", 0
	, "UInt", 0xCC0020)	; SRCCOPY						; copy processed DC
DllCall("SelectObject", "UInt", hDCm, "UInt", hOld)
DllCall("DeleteDC", "UInt", hDCm)
DllCall("EndPaint", "UInt", hwnd, "UInt", &ps)
DllCall("DeleteObject", "UInt", hBmp)
}
