This script was supposed to create and/or edit .bootskin files
used with the omonimous application by Stardock for setting
the boot image in Windows 2000/XP.

It is now obsolete, and falls into the legacy category.
It may still be useful to retro hobbyists.

The full-screen preview opens in a second monitor!

====================================

Unpacking is provided by Igor Pavlov's 7-zip archiver,
bundled with this application (current version is 9.22 beta).
http://www.7-zip.org
The archiver is covered by its own license.

These AHK scripts and the icons are licensed under GNU Public License v2 or later.

====================================

Change log:
=======
1.0.0.0 (2014.02.27)
- first alpha version, unreleased

1.0.0.1 (2014.03.04)
- added more comments
- added AlwaysOnTop menu checks and write to .ini
- unselect text in the Text dialog on open
- added fullscreen preview
- added palette display
- multiple fixes

. . .

1.0.0.8
- no code change, just moved additional modules to the lib folder

© Drugwash, 2014-2023
